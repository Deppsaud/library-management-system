package com.LMS.controller;

import com.LMS.dto.BorrowBookRequestDto;
import com.LMS.dto.BorrowBookResponseDto;
import com.LMS.response.Response;
import com.LMS.service.BorrowBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/borrow")
@Slf4j
public class BorrowBookController {

    @Autowired
    private BorrowBookService borrowBookService;

    @PostMapping("/add")
    public ResponseEntity<Response> addBorrow(@RequestBody BorrowBookRequestDto borrowBookRequestDto) {
        BorrowBookResponseDto borrow = borrowBookService.addBorrowBook(borrowBookRequestDto);
        Response<BorrowBookResponseDto> borrowDtoResponse = new Response<>(HttpStatus.CREATED, borrow);

        return new ResponseEntity<Response>(borrowDtoResponse, HttpStatus.CREATED);
    }

    @GetMapping("/id={id}")
    public ResponseEntity<Response> borrowBook(@PathVariable long id) {

        BorrowBookResponseDto borrow = borrowBookService.borrowBookById(id);
        Response<BorrowBookResponseDto> borrowBookResponseDto = new Response<>(HttpStatus.OK, borrow);
        return ResponseEntity.ok(borrowBookResponseDto);
    }


    @GetMapping("/return/{id}")
    public ResponseEntity<Response> returnBorrowBook(@PathVariable long id) {

        BorrowBookResponseDto returned = borrowBookService.returnBook(id);
        Response<BorrowBookResponseDto> borrowBookResponseDto = new Response<>(HttpStatus.OK, returned);
        return ResponseEntity.ok(borrowBookResponseDto);
    }

    @GetMapping("/getAll")
    public ResponseEntity<Response> getAllBorrow() {
        List<BorrowBookResponseDto> getAllBorrowHistory = borrowBookService.getAllBorrow();
        Response<List> borrowBookResponseDtoResponse = new Response<>(HttpStatus.OK, getAllBorrowHistory);
        return new ResponseEntity<Response>(borrowBookResponseDtoResponse, HttpStatus.OK);
    }

}