package com.LMS.excel;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/excel")
public class ExcelBookController {

    @Autowired
    private BookExcelService bookExcelService;

    @GetMapping("/book")
    public void generateExcelReport(HttpServletResponse response) throws Exception {

        response.setContentType("application/octet-stream");

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename = Books.xls";

        response.setHeader(headerKey, headerValue);

        bookExcelService.exportBook(response);

    }


}
