package com.LMS.excel;

import com.LMS.model.Book;
import com.LMS.repository.BookRepository;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;


@Service
public class BookExcelService {

@Autowired
    private BookRepository bookRepo;
   public void  exportBook(HttpServletResponse response) throws IOException {
       System.out.println("hello"+bookRepo.findAll());
        List<Book> books = bookRepo.findAll();
       System.out.println("hello"+books);
        //create a new workbook
        HSSFWorkbook workbook = new HSSFWorkbook();

        //create a new sheet
        HSSFSheet sheet = workbook.createSheet("Book");

        //create header row (Excel first row)
        HSSFRow headerRow = sheet.createRow(0);

        headerRow.createCell(0).setCellValue("ISBN");
        headerRow.createCell(1).setCellValue("Title");
        headerRow.createCell(2).setCellValue("Publication");
        headerRow.createCell(3).setCellValue("Author");
        headerRow.createCell(4).setCellValue("Quantity");


        int dataRowIndex = 1;
        for (Book book : books){
            HSSFRow dataRow = sheet.createRow(dataRowIndex);
            dataRow.createCell(0).setCellValue(book.getIsbn());
            dataRow.createCell(1).setCellValue(book.getTitle());
            dataRow.createCell(2).setCellValue(book.getPublication());
            dataRow.createCell(3).setCellValue(book.getAuthor().getName());
            dataRow.createCell(4).setCellValue(book.getQuantity());

            dataRowIndex ++;

        }
        ServletOutputStream ops = response.getOutputStream();
        workbook.write(ops);
        workbook.close();
        ops.close();
    }
}
