package com.LMS.model;

public enum Role {
    ROLE_STUDENT,
    ROLE_STAFF,
    ROLE_LIBRARIAN,
    ROLE_SUPER_ADMIN
}
