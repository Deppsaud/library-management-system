package com.LMS.serviceImpl;

import com.LMS.dto.UserRequestDto;
import com.LMS.dto.UserResponseDto;
import com.LMS.exception.AlreadyExistException;
import com.LMS.exception.NotFoundException;
import com.LMS.internationalization.LocaleResolverConfig;
import com.LMS.model.User;
import com.LMS.model.UserEnum;
import com.LMS.repository.UserRepository;
import com.LMS.service.UserService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Deepak Saud
 * this is a service class of user.
 */

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private LocaleResolverConfig localeResolverConfig;

    /**
     * this method is used to create a new user.
     * @param userRequestDto
     * @param language
     * @return
     */
    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto, String language) {

        Optional<User> userOptional = userRepo.findByEmail(userRequestDto.getEmail());

        String exceptionMessage;                                       //creating message for internationalization
            if(userOptional.isPresent()) {
                exceptionMessage = localeResolverConfig.getMessageSource().getMessage("exception.alreadyExist",null, new Locale(language));
//                System.out.println("error"+exceptionMessage);
                throw new AlreadyExistException(exceptionMessage);
            }
            User newUser = this.dtoToUser(userRequestDto);
            newUser.setStatus(UserEnum.ACTIVE);
            newUser.setPassword(BCrypt.hashpw(userRequestDto.getPassword(),BCrypt.gensalt()));
            return this.userToDTO(userRepo.save(newUser));         //converting user into userResponseDto

    }

    /**
     * this method is used to update existing user by id
     * @param userRequestDto
     * @param id
     * @return UserResponseDto
     */
    @Override
    public UserResponseDto updateUser(UserRequestDto userRequestDto, long id) {
        Optional<User> userOptional = userRepo.findById(userRequestDto.getId());
        if(!userOptional.isPresent()){
            throw new NotFoundException("User not Found");
       }
        if (userOptional.get().isPresentStatus() == false || userOptional.get().getStatus() == UserEnum.CLOSED) {
            throw new  NotFoundException("User cannot be accessed");
        }
        userOptional.get().setName(userRequestDto.getName());
        userOptional.get().setEmail(userRequestDto.getEmail());
        userOptional.get().setAddress(userRequestDto.getAddress());
        userOptional.get().setContactNo(userRequestDto.getContactNo());
        userOptional.get().setStatus(UserEnum.ACTIVE);
        userOptional.get().setPresentStatus(true);
        userOptional.get().setRole(userRequestDto.getRole());

        User updatedUser = userRepo.save(userOptional.get());

        UserResponseDto userResponseDto = new UserResponseDto();

        return userResponseDto;

    }

//----------------------------------Delete User -----------------------------------
    @Override
    public void deleteUser( long id) {
        Optional<User> optionalUser = userRepo.findById(id);
        if(!optionalUser.isPresent()){
            throw new NotFoundException("User not found");
        }
        optionalUser.get().setPresentStatus(false);
        optionalUser.get().setStatus(UserEnum.CLOSED);
        userRepo.save(optionalUser.get());
    }

    //############################## Get All User ###########################
    @Override
    public List<UserResponseDto> getAllUser() {

        List<User> activeUsers = userRepo.findByStatus(UserEnum.ACTIVE);
        if(activeUsers.isEmpty()){
            throw new NotFoundException("No Users are Present at available");
        }

        List<UserResponseDto> activeUserDtos = new ArrayList<>();

        for(User activeUser : activeUsers){
            activeUserDtos.add(userToDTO(activeUser));
        }
//        activeUsers.stream().forEach(activeUser->activeUserDtos.add(userToDTO(activeUser)));
        return activeUserDtos;
    }


//---------------------------------- excel sheet download -------------------------

public void generateExcel(HttpServletResponse response) throws IOException {
        List<User> users = userRepo.findAll();
        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet sheet = workbook.createSheet("User_info");

        HSSFRow row = sheet.createRow(0);

        row.createCell(0).setCellValue("ID");
        row.createCell(1).setCellValue("name");
        row.createCell(2).setCellValue("email");
        row.createCell(3).setCellValue("contactNo");
        row.createCell(4).setCellValue("address");
        row.createCell(5).setCellValue("role");


        int dataRowIndex = 1;

        for(User user : users){
            HSSFRow dataRow = sheet.createRow(dataRowIndex);
            dataRow.createCell(0).setCellValue(user.getId());
            dataRow.createCell(1).setCellValue(user.getName());
            dataRow.createCell(2).setCellValue(user.getEmail());
            dataRow.createCell(3).setCellValue(user.getContactNo());
            dataRow.createCell(4).setCellValue(user.getAddress());
            dataRow.createCell(5).setCellValue(user.getRole().toString());
            dataRowIndex ++;
        }

        ServletOutputStream ops = response.getOutputStream();
        workbook.write(ops);
        workbook.close();
        ops.close();


}


/**
 * find by id(long id)
 * @return UserResponseDto(user.get())
 */

    @Override
    public UserResponseDto getById(long id) {
        Optional<User> user = userRepo.findById(id);
            if(!user.isPresent())
            {
                throw new NotFoundException("user not found");
            }
            if(user.get().isPresentStatus()==false || user.get().getStatus()==UserEnum.CLOSED || user.get().getStatus()==null){
                throw new NotFoundException("User is Not Active Or The Account is Closed");
            }
                return new UserResponseDto(user.get());
    }

    /**
     * findAllByClosed
     *
     * @return closedUSerDtos
     */
    @Override
    public List<UserResponseDto> findAllByClosed() {

        List<User> closedUser = userRepo.findAllByStatus(UserEnum.CLOSED);
        System.out.println(closedUser);
        if(closedUser.isEmpty()){
            throw new NotFoundException("there is no any closed member are available");
        }

        List<UserResponseDto> closedUsersDtos = new ArrayList<>();

        for(User closed : closedUser){
            closedUsersDtos.add(userToDTO(closed));
        }

        return closedUsersDtos;
    }

    /* todo --------------------------------- create and update ----------------------------- */

    /**
     * dtoToUser
     * @param userDTO
     * @return dtoToUSer
     */
    public User dtoToUser(UserRequestDto userDTO){
        User newUser = new User();
        newUser.setName(userDTO.getName());
        newUser.setAddress(userDTO.getAddress());
        newUser.setEmail(userDTO.getEmail());
        newUser.setPassword(userDTO.getPassword());
        newUser.setContactNo(userDTO.getContactNo());
        newUser.setRole(userDTO.getRole());

        return newUser;
    }

    public UserResponseDto userToDTO(User user){
        UserResponseDto userDTORespond = new UserResponseDto();
        userDTORespond.setId(user.getId());
        userDTORespond.setName(user.getName());
        userDTORespond.setAddress(user.getAddress());
        userDTORespond.setEmail(user.getEmail());
        userDTORespond.setContactNo(user.getContactNo());
        userDTORespond.setRole(user.getRole());

        return userDTORespond;
    }
}
