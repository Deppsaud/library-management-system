package com.LMS.serviceImpl;

import ch.qos.logback.classic.spi.IThrowableProxy;
import com.LMS.dto.BorrowBookRequestDto;
import com.LMS.dto.BorrowBookResponseDto;
import com.LMS.dto.BorrowDto;
import com.LMS.exception.AlreadyExistException;
import com.LMS.exception.NotFoundException;
import com.LMS.model.Book;
import com.LMS.model.BorrowBook;
import com.LMS.model.User;
import com.LMS.repository.BookRepository;
import com.LMS.repository.BorrowBookRepository;
import com.LMS.repository.UserRepository;
import com.LMS.service.BorrowBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class BorrowBookServiceImpl implements BorrowBookService {

    @Autowired
    private BorrowBookRepository borrowBookRepo;
    @Autowired
    private BookRepository bookRepo;
    @Autowired
    private UserRepository userRepo;

    //-------------------------------- save borrow ---------------------------------
    @Override
    public BorrowBookResponseDto addBorrowBook(BorrowBookRequestDto borrowBookRequestDto) {
        Optional<Book> book = bookRepo.findByIsbn(borrowBookRequestDto.getIsbn());
//        System.out.println("isbn  number "+borrowBookRequestDto);
        if (!book.isPresent()) {
            throw new NotFoundException("Book is not found");
        }
           if(book.get().getQuantity()==0){
               throw new NotFoundException("book is out of stock");
           }

        BorrowBook toDoBorrowBook = DtoToBorrowBook(borrowBookRequestDto);

        Book todoBook = book.get();
        todoBook.setQuantity(todoBook.getQuantity()-1);

        bookRepo.save(todoBook);
       BorrowBook savedBorrow =  borrowBookRepo.save(toDoBorrowBook);
       return borrowBookToDto(savedBorrow);

    }

    //---------------------------------- return borrow ---------------------------------
    @Override
    public BorrowBookResponseDto returnBook(long id) {

      Optional<BorrowBook> returnBook =  borrowBookRepo.findById(id);
        if(!returnBook.isPresent())
        {
            throw new NotFoundException("Book is not taken ");
        }
        if(returnBook.get().getIsReturned()){
            throw new AlreadyExistException("already returned");
        }
        returnBook.get().setReturnedDate(LocalDate.now());
        returnBook.get().setIsReturned(true);

        Book book =returnBook.get().getBook();

        book.setQuantity(book.getQuantity()+1);
        book.setStatus(true);

        long daysBetween = ChronoUnit.DAYS.between(returnBook.get().getBorrowDate(), LocalDate.now());

        returnBook.get().setReservedDays(daysBetween);

        returnBook.get().setDueDays(0);

        if (daysBetween>15){
            returnBook.get().setDueDays((daysBetween-15));
            BigDecimal fine= BigDecimal.valueOf((daysBetween-15)*5);
            returnBook.get().setFine(fine);
        }




        bookRepo.save(book);
        borrowBookRepo.save(returnBook.get());

        BorrowBookResponseDto borrowBook = borrowBookToDto(returnBook.get());

        return borrowBook;
    }

    @Override
    public BorrowBookResponseDto borrowBookById(long id) {

        Optional<BorrowBook> borrowBook = borrowBookRepo.findById(id);

        if(!borrowBook.isPresent())
        {
            throw new NotFoundException("this transaction id not available");
        }

        return borrowBookToDto(borrowBook.get());



    }

    //---------------------- get all borrow -----------------------------
    @Override
    public List<BorrowBookResponseDto> getAllBorrow() {
       List<BorrowBook> getAllBorrow = borrowBookRepo.findAll();
       List<BorrowBookResponseDto> borrows=new ArrayList<>();
       getAllBorrow.stream().forEach(borrow->{
           borrows.add(borrowBookToDto(borrow));
       });
       return borrows;
    }




    //---------------------------- dto to borrow -----------------------

    public BorrowBook DtoToBorrowBook(BorrowBookRequestDto borrowBookRequestDto){
        BorrowBook borrowBook  = new BorrowBook();
        Book requestBook = bookRepo.findByIsbn(borrowBookRequestDto.getIsbn()).get();
        borrowBook.setBook(requestBook);
        User requestUser = userRepo.findById(borrowBookRequestDto.getUserId()).get();
        borrowBook.setUser(requestUser);

        return borrowBook;
    }

    //------------------------------ borrow to dto --------------------------------
    public BorrowBookResponseDto borrowBookToDto(BorrowBook borrowBook){
        BorrowBookResponseDto borrowBookResponseDto = new BorrowBookResponseDto();
        borrowBookResponseDto.setUserId(borrowBook.getUser().getId());
        borrowBookResponseDto.setIsbn(borrowBook.getBook().getIsbn());
        borrowBookResponseDto.setRequestDate(borrowBook.getBorrowDate());
        borrowBookResponseDto.setReturnDate(borrowBook.getReturnedDate());
        borrowBookResponseDto.setDueDays(borrowBook.getDueDays());
        borrowBookResponseDto.setReserveDays(borrowBook.getReservedDays());
        borrowBookResponseDto.setFine(borrowBook.getFine());

        return borrowBookResponseDto;
    }
}
