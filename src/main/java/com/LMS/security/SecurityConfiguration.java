package com.LMS.security;

import com.LMS.model.Role;
import com.LMS.model.UserEnum;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.logging.Logger;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration{

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{

               return http

                       .csrf()
                       .disable()
                       .authorizeHttpRequests()
                       .requestMatchers("/authenticate").permitAll()
                       .requestMatchers("/book/delete/*").hasRole("LIBRARIAN")
                       .requestMatchers("/book/save").hasRole("LIBRARIAN")
                       .requestMatchers("/book/**").hasRole("LIBRARIAN")
                       .requestMatchers("/book/fetchAll").hasAnyRole("LIBRARIAN","STAFF","STUDENT")
                       .requestMatchers("/book/fetch/*").hasAnyRole("LIBRARIAN","STAFF","STUDENT")
                       .requestMatchers("/user/*").hasRole("LIBRARIAN")
                       .requestMatchers("/user/update/id").hasAnyRole("LIBRARIAN","STAFF","STUDENT")
                       .requestMatchers("/user/fetchAll/*").hasRole("LIBRARIAN")
                       .requestMatchers("/borrow/add").hasRole("STUDENT")
                       .anyRequest().authenticated()
                       .and()
                       .httpBasic()
                       .and()
//                       .sessionManagement()
//                      .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                       .build();
//                       .and()
//
//                     .and()
//                     .authenticationProvider(authenticationProvider)
//                     .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)

    }

}
