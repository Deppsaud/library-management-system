package com.LMS.security;

import com.LMS.exception.NotFoundException;
import com.LMS.model.User;
import com.LMS.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {
    @Autowired
    private final UserRepository userRepository;


    @Bean
    public UserDetailsService userDetailsService(){
       return new UserDetailsService() {
           @Override
           public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
               if(!userRepository.findByEmail(email).isPresent()){
                    throw new UsernameNotFoundException("User not found!!!");
               }

               User user = userRepository.findByEmail(email).get();
               return new UserDetails() {
                   @Override
                   public Collection<? extends GrantedAuthority> getAuthorities() {
                       List<GrantedAuthority> authorities= new ArrayList<>();
                       authorities.add(new SimpleGrantedAuthority(user.getRole().toString()));
                       return authorities;
                   }

                   @Override
                   public String getPassword() {
                       return user.getPassword();
                   }

                   @Override
                   public String getUsername() {
                       return user.getEmail();
                   }

                   @Override
                   public boolean isAccountNonExpired() {
                       return true;
                   }

                   @Override
                   public boolean isAccountNonLocked() {
                       return true;
                   }

                   @Override
                   public boolean isCredentialsNonExpired() {
                       return true;
                   }

                   @Override
                   public boolean isEnabled() {
                       return user.isPresentStatus();
                   }
               };
           }
       };
    }

    //-------------------------- for matching user password and username ------------------
    @Bean
    public AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authen = new DaoAuthenticationProvider();
        authen.setPasswordEncoder(passwordEncoder());
        authen.setUserDetailsService(userDetailsService());
        return authen;
    }

    //------------------------- for passwordEncoding -------------------
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //-----------------------
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
}
