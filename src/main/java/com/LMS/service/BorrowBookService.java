package com.LMS.service;

import com.LMS.dto.BorrowBookRequestDto;
import com.LMS.dto.BorrowBookResponseDto;
import com.LMS.model.BorrowBook;
import org.springframework.stereotype.Service;

import java.util.List;

public interface BorrowBookService {
public BorrowBookResponseDto addBorrowBook(BorrowBookRequestDto borrowBookRequestDto);
    public BorrowBookResponseDto returnBook(long id);
    public BorrowBookResponseDto borrowBookById(long id);

    public List<BorrowBookResponseDto> getAllBorrow();

}
